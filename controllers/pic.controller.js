var Pic = require('../models/pic.model');
var assert = require('assert');

exports.create = function(req, res) {
    var pic = new Pic(
        {
        name: req.body.name,
        url: req.body.url,
        }
    );
    pic.save(function(err) {
        if(err) {
            return next(err);
        }
        res.redirect('/admin');
    })
};

exports.index = function(req, res) {
    var pics = Pic.find();
    res.render('./admin/admin', { title: 'Page d\'administration des images', 'pics': pics });
}