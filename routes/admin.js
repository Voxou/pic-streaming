var express = require('express');
var router = express.Router();
var picController = require('../controllers/pic.controller');

router.get('/', picController.index);

router.get('/create', function(req, res, next) {
    res.render('./admin/create', { title: 'Ajouter une image' });
});
router.post('/create', picController.create);

router.get('/edit', function(req, res, next) {
    res.render('./admin/edit', { title: 'Modifier une image' });
});

router.get('/delete', function(req, res, next) {
    res.render('./admin/delete', { title: 'Supprimer une image' });
});

module.exports = router;